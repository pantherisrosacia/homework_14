﻿#include <iostream>
#include <string>


int main()
{
    std::string Text = "Qwerty";

    std::cout << "Text - " << Text << "\n";
    std::cout << "Text length = " << Text.length() << "\n";
    std::cout << "First sign - " << Text[0] << "\n";
    std::cout << "Last sign - " << Text[Text.length() - 1];
}

